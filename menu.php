<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>
			Yellow Pizzas
		</title>
		<link href="styling/stylesheet.css" rel="stylesheet" type="text/css">
		<link href="styling/navbar.css" rel="stylesheet" type="text/css">
		<link href="styling/header.css" rel="stylesheet" type="text/css">
		<link href="styling/content.css" rel="stylesheet" type="text/css">
		<link href="styling/menu.css" rel="stylesheet" type="text/css">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<?php include 'php/navbar.php' ?>
		<div id="mainpane">
			<div id="header">
				Menukaart
			</div>
			<div id="content">
				<div class="text">
					Pizza's (<span class="spicy">pikant</span>, <span class="vega">vegetarisch</span>)
				</div>
				<div class="menu">
					<table>
						<tr>
							<td class="vega">
								MARGHERITA tomaten, kaas, mozzarella
							</td>
							<td class="vega">
								&euro;8,50
							</td>
						</tr>
						<tr>
							<td>
								NAPOLETANA<br>
								tomaten, kaas, ansjovis, kappertjes, mozzarella
							</td>
							<td>
								&euro;9,50
							</td>
						</tr>
						<tr>
							<td>
								SALAME<br>
								tomaten, kaas, salami
							</td>
							<td>
								&euro;9,50
							</td>
						</tr>
						<tr>
							<td>
								PROSCIUTTO<br>
								tomaten, kaas, ham
							</td>
							<td>
								&euro;9,50
							</td>
						</tr>
						<tr>
							<td class="vega">
								FUNGHI<br>
								tomaten, kaas, champignons
							</td>
							<td class="vega">
								&euro;9,25
							</td>
						</tr>
						<tr>
							<td>
								4 STAGIONI<br>
								tomaten, kaas, champignons, salami, ham, ansjovis, olijven, kappertjes
							</td>
							<td>
								&euro;11,75
							</td>
						</tr>
						<tr>
							<td>
								4 STAGIONI SPECIALE<br>
								tomaten, kaas, artisjokken, olijven, salami, ham, kappertjes, knoflook, champignons, ansjovis
							</td>
							<td>
								&euro;12,75
							</td>
						</tr>
						<tr>
							<td>
								DEL MARE<br>
								tomaten, kaas, zalm, ham
							</td>
							<td>
								&euro;10,75
							</td>
						</tr>
						<tr>
							<td>
								MISTA<br>
								tomaten, kaas, champignons, salami, ham, garnalen, zalm
							</td>
							<td>
								&euro;12,75
							</td>
						</tr>
						<tr>
							<td>
								CAPRICCIOSA<br>
								tomaten, kaas, garnalen, zalm, paprika, salami, ham
							</td>
							<td>
								&euro;12,75
							</td>
						</tr>
						<tr>
							<td>
								CAPRICCIOSA SPECIALE<br>
								tomaten, kaas, zalm, garnalen, paprika, ham, salami, champignons, uien, olijven, ansjovis
							</td>
							<td>
								&euro;14,75
							</td>
						</tr>
						<tr>
							<td class="spicy">
								SANTA LUCIA<br>
								tomaten, kaas, ham, salami, ansjovis, artisjokken
							</td>
							<td class="spicy">
								&euro;12,25
							</td>
						</tr>
						<tr>
							<td>
								SANTA LUCIA SPECIALE<br>
								tomaten, kaas, salami, ham, asperges, artisjokken, champignons, uien
							</td>
							<td>
								&euro;13,25
							</td>
						</tr>
						<tr>
							<td class="spicy">
								PEPERONE<br>
								tomaten, kaas, paprika, champignons, uien
							</td>
							<td class="spicy">
								&euro;11,75
							</td>
						</tr>
						<tr>
							<td>
								CANADESE<br>
								tomaten, kaas, verse paprika, ossehaas, spek, fricandeau, olijven, gorgonzola
							</td>
							<td>
								&euro;15,25
							</td>
						</tr>
						<tr>
							<td>
								DIANA<br>
								tomaten, kaas, gegrilde courgette, tonijn, gorgonzola
							</td>
							<td>
								&euro;13,00
							</td>
						</tr>
						<tr>
							<td>
								DEL CAVALIERE<br>
								tomaten, kaas, gerookte zalm, room, parmezaanse kaas
							</td>
							<td>
								&euro;13,25
							</td>
						</tr>
						<tr>
							<td>
								SAN MARCO<br>
								tomaten, kaas, gehakt in tomatensaus, fricandeau, asperges, paprikasaus, ananas, gorgonzola, parmezaanse kaas
							</td>
							<td>
								&euro;13,75
							</td>
						</tr>
						<tr>
							<td>
								CAMPAGNOLA<br>
								tomaten, kaas, paprika, uien, champignons, ham
							</td>
							<td>
								&euro;12,00
							</td>
						</tr>
						<tr>
							<td>
								CAMPANERA<br>
								tomaten, kaas, champignons, asperges, uien, ha
							</td>
							<td>
								&euro;12,00
							</td>
						</tr>
						<tr>
							<td>
								VULCANO<br>
								tomaten, kaas, champignons, ham
							</td>
							<td>
								&euro;11,50
							</td>
						</tr>
						<tr>
							<td>
								VESUVIO<br>
								tomaten, kaas, champignons, salami
							</td>
							<td>
								&euro;11,50
							</td>
						</tr>
						<tr>
							<td>
								CARBONARA<br>
								tomaten, kaas, salami, ham, fricandeau, spek
							</td>
							<td>
								&euro;12,25
							</td>
						</tr>
						<tr>
							<td>
								CARBONARA SPECIALE<br>
								tomaten, kaas, salami, ham, spek, ei, uien, fricandeau
							</td>
							<td>
								&euro;13,50
							</td>
						</tr>
						<tr>
							<td>
								CARBONARA ALLA GORGONZOLA<br>
								tomaten, kaas, salami, ham, spek, gorgonzola, peperkorrels
							</td>
							<td>
								&euro;14,00
							</td>
						</tr>
						<tr>
							<td>
								MONTANARA<br>
								tomaten, kaas, ham, salami, champignons
							</td>
							<td>
								&euro;11,75
							</td>
						</tr>
						<tr>
							<td>
								DI CIPOLLE<br>
								tomaten, kaas, champignons, uien
							</td>
							<td>
								&euro;11,25
							</td>
						</tr>
						<tr>
							<td>
								SARDAGNOLA<br>
								tomaten, kaas, salami, paprika, uien, champignons, olijven, ansjovis
							</td>
							<td>
								&euro;13,00
							</td>
						</tr>
						<tr>
							<td>
								PAESANA<br>
								tomaten, kaas, salami, ham, spek, uien, champignons
							</td>
							<td>
								&euro;13,25
							</td>
						</tr>
						<tr>
							<td>
								PAESANA GORGONZOLA<br>
								tomaten, kaas, salami, ham, spek, champignons, uien, gorgonzola, peperkorrels
							</td>
							<td>
								&euro;14,25
							</td>
						</tr>
						<tr>
							<td>
								SVEDESE<br>
								tomaten, kaas, ham, uien, ei
							</td>
							<td>
								&euro;12,00
							</td>
						</tr>
						<tr>
							<td>
								FANTASIA<br>
								fantasie van de kok
							</td>
							<td>
								&euro;14,75
							</td>
						</tr>
						<tr>
							<td>
								SALMONE E FUNGHI<br>
								tomaten, kaas, champignons, zalm
							</td>
							<td>
								&euro;11,75
							</td>
						</tr>
						<tr>
							<td>
								BURINA<br>
								tomaten, kaas, ham, asperges, artisjokken, paprikasaus
							</td>
							<td>
								&euro;12,25
							</td>
						</tr>
						<tr>
							<td>
								SAN FRANCESCO<br>
								tomaten, kaas, asperges, artisjokken, olijven, salami
							</td>
							<td>
								&euro;11,50
							</td>
						</tr>
						<tr>
							<td>
								CORSICANA<br>
								tomaten, kaas, olijven, kappertjes, ansjovis, knoflook
							</td>
							<td>
								&euro;11,25
							</td>
						</tr>
						<tr>
							<td class="spicy">
								DINAMITE<br>
								tomaten, kaas, paprika, uien, champignons, tonijn, ham, asperges
							</td>
							<td class="spicy">
								&euro;13,75
							</td>
						</tr>
						<tr>
							<td>
								MORA<br>
								tomaten, kaas, champignons, fricandeau, artisjokken, uien, asperges, salami, olijven
							</td>
							<td>
								&euro;13,50
							</td>
						</tr>
						<tr>
							<td>
								OLANDESE<br>
								tomaten, kaas, ei, spek, ham, uien, asperges
							</td>
							<td>
								&euro;13,00
							</td>
						</tr>
						<tr>
							<td>
								VENEZIANA<br>
								tomaten, kaas, tonijn, asperges, garnalen, ham, uien
							</td>
							<td>
								&euro;13,00
							</td>
						</tr>
						<tr>
							<td>
								VENEZIANA SPECIALE<br>
								tomaten, kaas, zalm, garnalen, tonijn, paprika, asperges, olijven
							</td>
							<td>
								&euro;14,75
							</td>
						</tr>
						<tr>
							<td class="spicy">
								INFERNO<br>
								tomaten, kaas, champignons, paprika, artisjokken, olijven
							</td>
							<td class="spicy">
								&euro;11,75
							</td>
						</tr>
						<tr>
							<td class="spicy">
								ROMANA<br>
								tomaten, kaas, champignons, uien, paprika, ham, spek, zalm, asperges, olijven
							</td>
							<td class="spicy">
								&euro;14,00
							</td>
						</tr>
						<tr>
							<td class="spicy">
								CIAO CIAO<br>
								tomaten, kaas, garnalen, gorgonzola, gehakt in tomatensaus, asperges, artisjokken, olijven, paprika, uien, knoflook
							</td>
							<td class="spicy">
								&euro;15,75
							</td>
						</tr>
						<tr>
							<td>
								TORINESE<br>
								tomaten, kaas, champignons, ham, salami, asperges, uien, olijven
							</td>
							<td>
								&euro;13,25
							</td>
						</tr>
						<tr>
							<td class="vega">
								VEGETARIANA<br>
								tomaten, kaas, champignons, asperges, olijven, artisjokken, paprika, uien
							</td>
							<td class="vega">
								&euro;12,75
							</td>
						</tr>
						<tr>
							<td class="vega">
								VEGETARIANA FRUTTA GORGONZOLA<br>
								tomaten, kaas, champignons, artisjokken, paprikasaus, paprika, uien, asperges, olijven, ananas, bananen, gorgonzola
							</td>
							<td class="vega">
								&euro;14,25
							</td>
						</tr>
						<tr>
							<td class="spicy">
								GORGONZOLA<br>
								tomaten, kaas, paprika, asperges, artisjokken, ham, uien, gorgonzola, knoflook
							</td>
							<td class="spicy">
								&euro;14,25
							</td>
						</tr>
						<tr>
							<td class="spicy">
								GORGONZOLA SPECIALE<br>
								tomaten, kaas, paprika, champignons, salami, ham, uien, asperges, artisjokken, olijven, gorgonzola, knoflook
							</td>
							<td class="spicy">
								&euro;14,75
							</td>
						</tr>
						<tr>
							<td>
								GORGONZOLA MOZZARELLA<br>
								tomaten, kaas, champignons, salami, ham, paprikasaus, asperges, artisjokken, olijven, gorgonzola, mozzarella
							</td>
							<td>
								&euro;14,75
							</td>
						</tr>
						<tr>
							<td>
								PIZZA ORTOLANA<br>
							</td>
							<td>
								&euro;16,25
							</td>
						</tr>
						<tr>
							<td>
								CALZONE (dubbelgeslagen)<br>
								dubbelgeslagen pizza met tomaten, kaas, salami, ham
							</td>
							<td>
								&euro;11,75
							</td>
						</tr>
						<tr>
							<td class="spicy">
								CALZONE SPECIALE (dubbelgeslagen)<br>
								tomaten, kaas, fricandeau, ham, salami, paprika, spek
							</td>
							<td class="spicy">
								&euro;13,75
							</td>
						</tr>
						<tr>
							<td class="spicy">
								CALZONE SPECIALE ALLA GORGONZOLA (dubbelgeslagen)<br>
								tomaten, kaas, ham, salami, spek, fricandeau, paprikasaus, gorgonzola
							</td>
							<td class="spicy">
								&euro;14,25
							</td>
						</tr>
						<tr>
							<td>
								CALZONE Al FRUTTI DI MARE (dubbelgeslagen)<br>
								tomaten, kaas, tonijn, uien, zalm, garnalen, ansjovis, paling
							</td>
							<td>
								&euro;14,50
							</td>
						</tr>
						<tr>
							<td>
								CALZONE ALLA SICILIANA (dubbelgeslagen)<br>
								tomaten, kaas, ham, oesterzwammen, tonijn, uien, kappertjes
							</td>
							<td>
								&euro;14,25
							</td>
						</tr>
						<tr>
							<td>
								PIZZA DELLA FRIVOLEZZA<br>
								tomaten, kaas, plakjes ossenhaas, gruyère, knoflook, spek
							</td>
							<td>
								&euro;14,50
							</td>
						</tr>
						<tr>
							<td>
								PIZZA GONDOLIERE<br>
								tomaten, kaas, gesneden varkensfilet, salami, spek, champignons, rozemarijn, peperkorrels
							</td>
							<td>
								&euro;14,50
							</td>
						</tr>
						<tr>
							<td>
								PIZZA ALLA PIZZAIOLA<br>
								tomaten, mozzarella, ossehaas, kappertjes, olijven, oregano, basilicum, peperkorrels
							</td>
							<td>
								&euro;14,50
							</td>
						</tr>
						<tr>
							<td>
								PIZZA RUSTICA NAPOLETANA<br>
								tomaten, kaas, mozzarella, ham, salami, ei, parmezaanse kaas, basilicum
							</td>
							<td>
								&euro;13,50
							</td>
						</tr>
						<tr>
							<td class="spicy">
								PIZZA PAELLA ALL'ITALIANA<br>
								tomaten, kaas, garnalen, zalm, mosselen, paling, zalm, paprika, artisjokken, ham, salami, uien
							</td>
							<td class="spicy">
								&euro;16,00
							</td>
						</tr>
						<tr>
							<td>
								PIZZA ANANAS E FORMAGGI<br>
								tomaten, kaas, ham, ananas, gruyère, gorgonzola, mozzarella, paprika zoet en zuur met gehakt vlees en olijven
							</td>
							<td>
								&euro;15,75
							</td>
						</tr>
						<tr>
							<td>
								PIZZA SAN VALENTINO<br>
								tomaten, kaas, ananas, ham, mozzarella, gorgonzola, paprika , ossehaas, varkenshaas, groene asperges
							</td>
							<td>
								&euro;16,00
							</td>
						</tr>
						<tr>
							<td>
								PIZZA AL PARMIGIANO<br>
								tomaten, kaas, ham, salami, parmezaanse kaas, gruyère, mascarpone, olijven, paprika, peper korrels
							</td>
							<td>
								&euro;15,00
							</td>
						</tr>
						<tr>
							<td>
								PIZZA BARBARESCA<br>
								tomaten, kaas, gehakt en parmezaanse kaas
							</td>
							<td>
								&euro;11,25
							</td>
						</tr>
						<tr>
							<td>
								MAFIOSA<br>
								tomaten, kaas, ham, gehakt in tomensaus, paprika, champignons, parmezaanse kaas, gorgonzola en olijven
							</td>
							<td>
								&euro;13,50
							</td>
						</tr>
						<tr>
							<td>
								AMERICANA<br>
								tomaten, kaas, gehakt in tomatensaus, spek, uien, paprika, champignons, parmezaanse kaas
							</td>
							<td>
								&euro;13,75
							</td>
						</tr>
						<tr>
							<td>
								MERAVIGLIOSA<br>
								tomaten, kaas, fricandeau, asperges, paprika, champignons, ham, uien, parmezaanse kaas
							</td>
							<td>
								&euro;13,00
							</td>
						</tr>
						<tr>
							<td>
								4 FORMAGGI<br>
								tomaten, kaas, ham, gorgonzola, mozzarella, mascarpone, gruyère, peperkorrels
							</td>
							<td>
								&euro;14,25
							</td>
						</tr>
						<tr>
							<td class="spicy">
								DELL' AMORE<br>
								tomaten, kaas, ham, ananas, gorgonzola, champignons, paprika, ossehaas, parmezaanse kaas
							</td>
							<td class="spicy">
								&euro;14,75
							</td>
						</tr>
						<tr>
							<td class="spicy">
								DEL RE<br>
								tomaten, kaas, ossehaas, fricandeau, salami, spek, ham, ananas, paprikasaus, champignons
							</td>
							<td class="spicy">
								&euro;14,75
							</td>
						</tr>
						<tr>
							<td class="spicy">
								AL CAPONE<br>
								tomaten, kaas, gorgonzola, champignons, ossehaas, salami, paprika, asperges, parmezaanse kaas
							</td>
							<td class="spicy">
								&euro;14,50
							</td>
						</tr>
						<tr>
							<td class="spicy">
								DEL BANDITO<br>
								tomaten, kaas, champignons, gehakt in tomatensaus, ossehaas, salami, asperges, gorgonzola, parmezaanse kaas
							</td>
							<td class="spicy">
								&euro;14,50
							</td>
						</tr>
						<tr>
							<td>
								PADRINO<br>
								tomaten, kaas, champignons, ham, gehakt in tomatensaus, artisjokken, fricandeau, parmaham, parmezaansekaas, gorgonzola, mozzarella en gruyere.
							</td>
							<td>
								&euro;14,50
							</td>
						</tr>
						<tr>
							<td class="spicy">
								DEL BONGUSTAIO<br>
								tomaten, kaas, ossehaas, champignons, fricandeau, paprika, asperges, salami, ananas, gorgonzola, parmezaanse kaas
							</td>
							<td class="spicy">
								&euro;15,25
							</td>
						</tr>
						<tr>
							<td>
								MERIDIONALE<br>
								tomaten, kaas, champignons, paprikasaus, ossehaas, gorgonzola, parmezaanse kaas, peperkorrels, knoflook
							</td>
							<td>
								&euro;14,75
							</td>
						</tr>
						<tr>
							<td class="spicy">
								BOSCAIOLA<br>
								tomaten, kaas, ossehaas, plakjes varkenshaas, oesterzwammen, spek, gorgonzola, mozzarella, paprikasaus, knoflook
							</td>
							<td class="spicy">
								&euro;16,25
							</td>
						</tr>
						<tr>
							<td class="spicy">
								DEL CACCIATORE<br>
								tomaten, kaas, champignons, ossehaas, plakjes varkenshaas, ham, salami, fricandeau, spek, uien, gorgonzola, mozzarella
							</td>
							<td class="spicy">
								&euro;16,00
							</td>
						</tr>
						<tr>
							<td>
								ALLA DIAVOLA<br>
								tomaten, kaas, oesterzwammen, ossehaas, gorgonzola, groene asperges, fricandeau, verse paprika, ham, uien
							</td>
							<td>
								&euro;15,25
							</td>
						</tr>
						<tr>
							<td class="spicy">
								MARE E MONTI<br>
								tomaten, kaas, zalm, garnalen, tonijn, oesterzwammen, groene asperges, uien
							</td>
							<td class="spicy">
								&euro;15,75
							</td>
						</tr>
						<tr>
							<td class="spicy">
								ALLA MASCARPONE<br>
								tomaten, kaas, mascarpone, oesterzwammen, gorgonzola, ossehaas, salami, fricandeau, ham, verse paprika, uien
							</td>
							<td class="spicy">
								&euro;15,75
							</td>
						</tr>
						<tr>
							<td class="spicy">
								PRIMAVERA<br>
								tomaten, kaas, oesterzwammen, ossehaas, varkenshaas, uien, gorgonzola, brie, mozzarella, peterselie, verse tomaat
							</td>
							<td class="spicy">
								&euro;16,00
							</td>
						</tr>
						<tr>
							<td>
								SAN PIETRO<br>
								tomaten, kaas, artisjokken, ossehaas, ham, oesterzwammen, courgette, gorgonzola
							</td>
							<td>
								&euro;13,75
							</td>
						</tr>
						<tr>
							<td>
								ITALIANA<br>
								tomaten, kaas, ossehaas, oesterzwammen, broccoli, gorgonzola, spek
							</td>
							<td>
								&euro;13,50
							</td>
						</tr>
						<tr>
							<td>
								4 MORI<br>
								tomaten, kaas, broccoli, oesterzwammen, gruyère, gorgonzola
							</td>
							<td>
								&euro;14,00
							</td>
						</tr>
						<tr>
							<td>
								ALLA PESCATORE<br>
								tomaten, kaas, tonijn
							</td>
							<td>
								&euro;10,50
							</td>
						</tr>
						<tr>
							<td>
								CAPRESE<br>
								tomaten, kaas, zalm, garnalen, inktvis, ansjovis
							</td>
							<td>
								&euro;12,75
							</td>
						</tr>
						<tr>
							<td>
								STELLA MARINA<br>
								tomaten, kaas, garnalen, zalm, asperges
							</td>
							<td>
								&euro;12,25
							</td>
						</tr>
						<tr>
							<td>
								ADRIATICA<br>
								tomaten, kaas, tonijn, garnalen, ansjovis
							</td>
							<td>
								&euro;12,25
							</td>
						</tr>
						<tr>
							<td>
								FRUTTI DI MARE<br>
								tomaten, kaas, zalm, mosselen, tonijn, garnalen, ansjovis
							</td>
							<td>
								&euro;13,50
							</td>
						</tr>
						<tr>
							<td>
								MARINARA<br>
								tomaten, kaas, zalm, garnalen, ansjovis
							</td>
							<td>
								&euro;12,00
							</td>
						</tr>
						<tr>
							<td class="spicy">
								FUNGHI ALLA MARINARA<br>
								tomaten, kaas, champignons, garnalen, zalm, mosselen
							</td>
							<td class="spicy">
								&euro;12,75
							</td>
						</tr>
						<tr>
							<td>
								MOZZARELLA FRUTTI DI MARE<br>
								tomaten, kaas, tomaten, kaas, mozzarella, zalm, garnalen, tonijn, paling, uien, inktvis, peterselie
							</td>
							<td>
								&euro;15,25
							</td>
						</tr>
						<tr>
							<td class="spicy">
								FANTASIA DI MARE<br>
								tomaten, kaas, inktvis, mosselen, tonijn, zalm, garnalen, ansjovis, uien
							</td>
							<td class="spicy">
								&euro;14,50
							</td>
						</tr>
						<tr>
							<td>
								DELICATEZZA MARINA<br>
								tomaten, kaas, gamba's, zalm filet, zalm, gerookte zalm, mozzarella, tonijn, paprikasaus, basilicum
							</td>
							<td>
								&euro;16,00
							</td>
						</tr>
						<tr>
							<td class="spicy">
								MEDITERRANIA<br>
								tomaten, kaas, zalm filet, inktvis, gamba's, tonijn, gorgonzola, mozzarella, knoflook, peterselie, verse tomaat
							</td>
							<td class="spicy">
								&euro;15,75
							</td>
						</tr>
						<tr>
							<td>
								CALIFORNIA<br>
								tomaten, kaas, abrikozen, banaan, ananas, meloen, mozzarella, gorgonzola, brie
							</td>
							<td>
								&euro;13,25
							</td>
						</tr>
						<tr>
							<td>
								GAMBERI ALLA FRUTTA<br>
								tomaten, kaas, mozzarella, ananas, banaan, garnalen
							</td>
							<td>
								&euro;12,50
							</td>
						</tr>
						<tr>
							<td>
								TUTTI FRUTTI TROPICALE<br>
								tomaten, kaas, banaan, abrikozen, ananas, spek, gorgonzola
							</td>
							<td>
								&euro;13,00
							</td>
						</tr>
						<tr>
							<td>
								HAWAII<br>
								tomaten, kaas, ananas en ham
							</td>
							<td>
								&euro;10,50
							</td>
						</tr>
					</table>
				</div>
				<div class="text">
					Salades (<span class="vega">vegetarisch</span>)
				</div>
				<div class="menu">
					<table>
						<tr>
							<td>
								Vis<br>
								Maaltijdsalade gerookte zalm, forel, gerookte makreel en pangsit van gamba's
							</td>
							<td>
								&euro;18,50
							</td>
						</tr>
						<tr>
							<td class="vega">
								Geitenkaas<br>
								Maaltijdsalade van geitenkaas, tomaat, rucola, olijven en gemengde noten
							</td>
							<td class="vega">
								&euro;13,50
							</td>
						</tr>
						<tr>
							<td>
								Krokante kip<br>
								Maaltijdsalade met krokant gebakken kip en gembermayonaise
							</td>
							<td>
								&euro;15,50
							</td>
						</tr>
						<tr>
							<td>
								Carpaccio<br>
								Maaltijdsalade van rundercarpaccio met tomaat, parmezaan, pijnboompitten en pesto-olie
							</td>
							<td>
								&euro;14,50
							</td>
						</tr>
						<tr>
							<td class="vega">
								Mozzarella<br>
								Maaltijdsalade van buffelmozzarella, tomaat, verse basilicum en pesto-olie
							</td>
							<td class="vega">
								&euro;13,50
							</td>
						</tr>
						<tr>
							<td>
								Gerookte rib-eye<br>
								Maaltijdsalade met gerookt rib-eye en dressing van gepofte knoflook
							</td>
							<td>
								&euro;15,50
							</td>
						</tr>
					</table>
				</div>
			</div>
			<?php include 'php/footer.php' ?>
		</div>
	</body>
</html>