<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>
			Yellow Pizzas
		</title>
		<link href="styling/stylesheet.css" rel="stylesheet" type="text/css">
		<link href="styling/navbar.css" rel="stylesheet" type="text/css">
		<link href="styling/header.css" rel="stylesheet" type="text/css">
		<link href="styling/content.css" rel="stylesheet" type="text/css">
		<link href="styling/open.css" rel="stylesheet" type="text/css">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<?php include 'php/navbar.php' ?>
		<div id="mainpane">
			<div id="header">
				Openingstijden
			</div>
			<div id="content">
				<div class="text">
					Wij zijn geopend op de volgende momenten:<br>
				</div>
				<table>
					<tr>
						<td>
							Maandag
						</td>
						<td>
							17:00 - 21:00
						</td>
					</tr>
					<tr>
						<td>
							Dinsdag
						</td>
						<td>
							17:00 - 21:00
						</td>
					</tr>
					<tr>
						<td>
							Woensdag
						</td>
						<td>
							17:00 - 21:00
						</td>
					</tr>
					<tr>
						<td>
							Donderdag
						</td>
						<td>
							17:00 - 23:00
						</td>
					</tr>
					<tr>
						<td>
							Vrijdag
						</td>
						<td>
							17:00 - 23:00
						</td>
					</tr>
					<tr>
						<td>
							Zaterdag
						</td>
						<td>
							17:00 - 23:00
						</td>
					</tr>
					<tr>
						<td>
							Zondag
						</td>
						<td>
							19:00 - 23:00
						</td>
					</tr>
				</table>
			</div>
			<?php include 'php/footer.php' ?>
		</div>
	</body>
</html>