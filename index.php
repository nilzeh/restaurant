<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>
			Yellow Pizzas
		</title>
		<link href="styling/stylesheet.css" rel="stylesheet" type="text/css">
		<link href="styling/navbar.css" rel="stylesheet" type="text/css">
		<link href="styling/frontheader.css" rel="stylesheet" type="text/css">
		<link href="styling/frontcontent.css" rel="stylesheet" type="text/css">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<?php include 'php/navbar.php' ?>
		<div id="mainpane">
			<div id="header">
				<div id="hleft">
					<img alt="Yellow Pizza logo" id="homelogo" src="img/logo.svg">
				</div>
				<div id="hright">
					Yellow Pizzas
				</div>
			</div>
			<div id="content">
				<div class="text">
					Welkom bij Yellow Pizzas! 
					<br> <br>
					Bij ons vindt u een uitgebreid assortiment aan goed belegde pizza's tegen een lage prijs. Daarnaast verkopen we een aantal frisse salades. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In semper augue vel ultricies semper. Donec auctor tempor posuere. Donec ligula ipsum, porttitor eu cursus sed, porta in arcu. Nullam porttitor dictum quam, et suscipit sapien varius vel. Proin blandit pulvinar nisl vel gravida. Fusce a mollis metus. Aliquam id justo vel sem malesuada mollis id sed nulla. Cras placerat, risus vitae malesuada vestibulum, elit est molestie justo, eu bibendum nisi mauris vitae urna. Fusce sed sodales erat. Mauris a nisi cursus, posuere lacus sed, aliquam risus. In rutrum euismod nisl, hendrerit ullamcorper lorem eleifend non. Praesent eu scelerisque urna, eu feugiat orci. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eu justo ut nisi egestas lobortis. 
					<br> <br>
					Als 50 jaar suspendisse sagittis odio at sagittis luctus. Praesent sit amet tortor et enim convallis porta nec ac purus. Mauris non lacus vel risus pellentesque lacinia. In et leo sed dolor gravida pulvinar quis sit amet magna. In vehicula quis purus adipiscing sodales. Duis lacus elit, lacinia at risus varius, ultricies vehicula lorem. Praesent fermentum, risus a viverra commodo, turpis lacus imperdiet dui, vel hendrerit elit purus ac purus. Donec adipiscing cursus risus, non semper sapien.
					<br> <br>
					We zien u graag in ons restaurant!
				</div>
			</div>
			<?php include 'php/footer.php' ?>
		</div>
	</body>
</html>