<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>
			Yellow Pizzas
		</title>
		<link href="styling/stylesheet.css" rel="stylesheet" type="text/css">
		<link href="styling/navbar.css" rel="stylesheet" type="text/css">
		<link href="styling/header.css" rel="stylesheet" type="text/css">
		<link href="styling/content.css" rel="stylesheet" type="text/css">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<?php include 'php/navbar.php' ?>
		<div id="mainpane">
			<div id="header">
				Links
			</div>
			<div id="content">
				<div class="text">
					Voor deze website heb ik een aantal websites geraadpleegd en/of ter inspiratie gebruikt:
					<ul>
						<li><a href="http://colorschemedesigner.com/">Color Scheme Designer</a>, voor het kleurenschema, op 85 graden.</li>
						<li><a href="http://www.color-hex.com/">Color Hex</a>, voor het varieren van tinten van het kleurenschema.</li>
						<li><a href="https://bitbucket.org/">Bitbucket</a>, voor locatie en inrichting van de navigatiebalk.</li>
						<li><a href="http://img.bhs4.com/c3/e/c3e44e013731411b0c57f41fe3e8932d70c90345_large.jpg">Abstracte pizza</a>, voor kleuren en vorm van het logo.</li>
						<li><a href="http://www.contini.nl/html/menu_pizze.htm">Contini</a>, voor pizza's.</li>
						<li><a href="http://www.rendezvous.nl/#/menukaart/salades">Rendezvous</a>, voor salades.</li>
					</ul>
					Wijzigingen kunnen gevolgd worden via de <a href="https://bitbucket.org/nilzeh/restaurant/commits/all">repository</a> van deze website.
				</div>
			</div>
			<?php include 'php/footer.php' ?>
		</div>
	</body>
</html>

