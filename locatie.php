<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>
			Yellow Pizzas
		</title>
		<link href="styling/stylesheet.css" rel="stylesheet" type="text/css">
		<link href="styling/navbar.css" rel="stylesheet" type="text/css">
		<link href="styling/header.css" rel="stylesheet" type="text/css">
		<link href="styling/content.css" rel="stylesheet" type="text/css">
		<link href="styling/map.css" rel="stylesheet" type="text/css">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<?php include 'php/navbar.php' ?>
		<div id="mainpane">
			<div id="header">
				Locatie
			</div>
			<div id="content">
				<div class="text">
					U kun ons vinden in Groningen aan de Singelweg 73. U kunt ons bereiken door te bellen naar 050-1234567 of door te mailen naar <a href="mailto:yellow@piz.za">yellow@piz.za</a>, om bijvoorbeeld uw bestelling door te geven of een tafel te reserveren.<br><br>
				</div>

				<iframe src="https://maps.google.nl/maps?t=m&amp;q=Singelweg+73,+9714+AS+Groningen&amp;ie=UTF8&amp;hq=&amp;hnear=Singelweg+73,+9714+AS+Korrewegwijk,+Groningen&amp;ll=53.232602,6.566133&amp;spn=0.025688,0.04283&amp;z=14&amp;iwloc=A&amp;output=embed">Kaartlocatie</iframe>
			</div>
			<?php include 'php/footer.php' ?>
		</div>
	</body>
</html>
